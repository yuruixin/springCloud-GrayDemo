package com.demo.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;


/**
 * Email miles02@163.com
 *
 * @author fangzhipeng
 * create 2018-11-16
 **/
public class TokenFilter implements GlobalFilter, Ordered {

    public static ThreadLocal<String> serviceVersionTL = new ThreadLocal<>();

    public static Map<String,String> mappingList = new HashMap<>();

    static {
        mappingList.put("商户1","1.0");
        mappingList.put("商户2","2.0");
    }

    Logger logger=LoggerFactory.getLogger( TokenFilter.class );
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        serviceVersionTL.remove();
        String token = exchange.getRequest().getQueryParams().getFirst("token");
        if (token == null || token.isEmpty()) {
            logger.info( "token is empty..." );
            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
            return exchange.getResponse().setComplete();
        }
        // TODO: 此处应通过token解析出对应的商户id，然后通过商户id，获取到对应服务版本
        String serviceVersion = mappingList.get("商户2");
        //此处通过直接模拟将token转换
        serviceVersionTL.set(serviceVersion);
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -100;
    }
}

